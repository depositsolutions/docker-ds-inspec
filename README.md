# Custom Inspec Build Container
[![pipeline status](https://gitlab.com/depositsolutions/docker-ds-inspec/badges/master/pipeline.svg)](https://gitlab.com/depositsolutions/docker-ds-inspec/commits/master)

This project contains the dockerfile for a custom ruby base image of gitlab-ci jobs.

## Dependencies
  * Installed packages for compliance testing (inspec)

## HowTo
Build docker image to update the included packages
```
docker build . -t  registry.gitlab.com/depositsolutions/docker-ds-inspec:latest
```

Upload image to the registry
```
docker push registry.gitlab.com/depositsolutions/docker-ds-inspec:latest
```

Use image in gitlab-ci.yml
```
image: registry.gitlab.com/depositsolutions/docker-ds-inspec:latest
```

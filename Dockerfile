FROM ruby:2.6.0-stretch

RUN echo gem: --no-document > $HOME/.gemrc
COPY Gemfile* /tmp/
WORKDIR /tmp
RUN bundler install
